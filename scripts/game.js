/*********************/
/*     SETTINGS      */
/*********************/
// Settings for movement of the main actor
var maxMovementSpeedX = 0.0005;
var maxMovementSpeedZ = 0.0007;
//var maxMovementSpeedZ = 0.001;
//var maxMovementSpeedX = 0.001;
var limitMovementX = 1.5;
var limitMovementBack = 3.3;
var limitMovementForward = -11.7;

// Settings for Camera Positioning
var cameraBack = 0.5;
var cameraUp = 0.67;

var fps = 40;

var movementSpeedPesecZ = 0.00015;
var movementSpeedPesecX = 0;

// Pozicioniranje modelov na igralno površino
// ------------ za shranjevanje .obj objektov (v app.meshes.<ime-objekta>) -------------
var app = {};
	app.meshes = {};
	app.pos = {};
	app.pos.player = [
		{
			scale: [0.3, 0.3, 0.3],
			rotY: degToRad(180)
		}
	];
	app.pos.actor = [
		{// Kompleten z vsemi mogočimi vrednostmi !!!
			scale: [0.01, 0.01, 0.01],
			x: 0.0,
			y: 0.0,
			z: -2.0,
			//rotX: degToRad(0),
			//rotY: degToRad(0),
			//rotZ: degToRad(0),
			//limitX: 0.19,		// Omejitev kvadrata od položaja + offsetX v obe smeri X
			//limitZ: 0.11,		// Omejtiev kvadrata od položaja + offsetZ v obe smeri Z
			//offsetX: -0.01,	// Negativna vrednost pomeni premik omejitve v levo !!
			//offsetZ: -0.4		// Negativna vrednost pomeni premik omejitve noter (naprej od player-ja) !!
								// Vrednosti offset se uporabljajo iste za vse pojavitve (razen v primeru drugačnega scale)
		}
		
	];
	app.pos.lichking = [
		{
			scale: [0.9, 0.9, 0.9],
			x : 0.0,
			z : -10.0,
			rotY: degToRad(270),
			limitX: 0.40,
			limitZ: 0.15,
			offsetX: -0.02,
			offsetZ: -0.52
		}
	];
	app.pos.lara = [
		{
			scale: [0.33, 0.33, 0.33],
			x: 1.0,
			z: -2.0,
			limitX: 0.21,
			limitZ: 0.07,
			//offsetX: 0.2,	// Negativna vrednost pomeni premik omejitve v levo !!
			offsetZ: -0.52
		},
		{
			scale: [0.33, 0.33, 0.33],
			x: -0.2,
			z: -7.0,
			limitX: 0.21,
			limitZ: 0.07,
			//offsetX: 0.2,	// Negativna vrednost pomeni premik omejitve v levo !!
			offsetZ: -0.52
		}
	];
	app.pos.alfred = [
		{
			scale: [0.07, 0.07, 0.07],
			x: -1.3,
			z: -5,
			rotZ: degToRad(0),
			limitX: 0.24,
			limitZ: 0.1,
			offsetZ: -0.5
		}
	];
	app.pos.ana = [
		{
			scale: [0.07, 0.07, 0.07],
			x: 1.5,
			z: -5,
			rotY: degToRad(270),
			rotZ: degToRad(0),
			limitX: 0.2,
			limitZ: 0.3,
			//offsetX: 0.2,
			offsetZ: -0.5
		}
		
	
	];
	app.pos.zid = [
		// zadnja stran
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.2,
			z: 4.655,
			rotY: degToRad(180)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.2,
			z: 4.655,
			rotY: degToRad(180)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.2+1+0.2,
			z: 4.655,
			rotY: degToRad(180)
		},
		// leva stran
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: 0,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: 1,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: -1,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: -2,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: 2,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: 3,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: 4,
			rotY: degToRad(270),
			rotZ: degToRad(0),

		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: -3,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: -4,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: -6,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: -7,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: -8,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: -9,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: -10,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: -1.7,
			z: -11,
			rotY: degToRad(270)
		},
		// desna stran
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: 0,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: 1,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: -1,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: -2,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: 2,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: 3,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: 4,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: -3,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: -4,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: -5,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: -6,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: -7,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: -8,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: -9,
			rotY: degToRad(270)
		},
		{
			scale: [0.1, 0.1, 0.1],
			x: 1.7,
			z: -10,
			rotY: degToRad(270)
		}
	
	];
	app.pos.mek = [
		{
			scale: [0.35, 0.35, 0.35],
			x: 0,
			y: 0,
			z: -15,
			limitX: 0.4,
			limitZ: 0.4
		}
		
	
	];
	app.pos.trash = [
		// levo
		{
			scale: [0.0045, 0.0045, 0.0045],
			x: -1.7,
			y: 0.3,
			z: -3,
			limitX: 0.4,
			limitZ: 0.4,
			offsetX: 0.2,
			offsetZ: -0.65
		},
		{
			scale: [0.0045, 0.0045, 0.0045],
			x: -1.7,
			y: 0.3,
			z: -3.5,
			limitX: 0.4,
			limitZ: 0.4,
			offsetX: 0.2,
			offsetZ: -0.65
		},
		// trije levo
		{
			scale: [0.0045, 0.0045, 0.0045],
			x: -1.7,
			y: 0.3,
			z: -7.4,
			limitX: 0.4,
			limitZ: 0.4,
			offsetX: 0.2,
			offsetZ: -0.65
		},
		{
			scale: [0.0045, 0.0045, 0.0045],
			x: -1.3,
			y: 0.3,
			z: -7.5,
			limitX: 0.4,
			limitZ: 0.4,
			offsetX: 0.2,
			offsetZ: -0.65
		},
		{
			scale: [0.0045, 0.0045, 0.0045],
			x: -0.9,
			y: 0.3,
			z: -7.6,
			limitX: 0.4,
			limitZ: 0.4,
			offsetX: 0.2,
			offsetZ: -0.65
		},
		// desno
		{
			scale: [0.0045, 0.0045, 0.0045],
			x: 1.0,
			y: 0.32,
			z: -6,
			limitX: 0.4,
			limitZ: 0.4,
			offsetX: 0.4,
			offsetZ: -0.65
		},
		{
			scale: [0.0045, 0.0045, 0.0045],
			x: 0.6,
			y: 0.32,
			z: -6.1,
			limitX: 0.4,
			limitZ: 0.4,
			offsetX: 0.4,
			offsetZ: -0.65
		},
		{
			scale: [0.0045, 0.0045, 0.0045],
			x: 0.2,
			y: 0.32,
			z: -6.2,
			limitX: 0.4,
			limitZ: 0.4,
			offsetX: 0.4,
			offsetZ: -0.65
		},

	];
	app.pos.klop = [
		{
			scale: [0.02, 0.02, 0.02],
			x: -1.2,
			y: -0.1,
			z: -1,
			rotX: degToRad(270),
			rotZ: degToRad(180),
			limitX: 0.3,
			limitZ: 0.45,
			offsetX: -0.15,
			offsetZ: -0.2
		},
		{
			scale: [0.02, 0.02, 0.02],
			x: 1.2,
			y: -0.1,
			z: -1,
			rotX: degToRad(270),
			limitX: 0.3,
			limitZ: 0.45,
			offsetX: 0.15,
			offsetZ: -0.5
		},
		{
			scale: [0.02, 0.02, 0.02],
			x: 1.2,
			y: -0.1,
			z: -4,
			rotX: degToRad(270),
			limitX: 0.3,
			limitZ: 0.45,
			offsetX: 0.15,
			offsetZ: -0.5
		},
		{
			scale: [0.02, 0.02, 0.02],
			x: -1.2,
			y: -0.1,
			z: -6,
			rotX: degToRad(270),
			rotZ: degToRad(180),
			limitX: 0.3,
			limitZ: 0.45,
			offsetX: -0.15,
			offsetZ: -0.2
		}
		
	
	];
	/*app.pos.luc = [
		{
			scale: [0.01, 0.01, 0.01],
			x: 0,
			y: 0,
			z: 0,
			rotX: degToRad(270),
			rotZ: degToRad(180),
			rotY: degToRad(0),
			limitX: 0.4,
			limitZ: 0.4
		}
		
	
	];*/
	app.pos.car = [
		{
			scale: [0.005, 0.005, 0.005],
			x: 0,
			y: 0.36,
			z: -5,
			rotY: degToRad(180),
			limitX: 0.55,
			limitZ: 1.1,
			offsetZ: -0.6
		}
		
	
	];
	app.pos.house = [
		{
			scale: [0.15, 0.15, 0.15],
			x: -4,
			y: 1.3,
			z: -1,
			rotX: degToRad(270),
			rotZ: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -4,
			y: 1.3,
			z: -4,
			rotX: degToRad(270),
			rotZ: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -4,
			y: 1.3,
			z: 2,
			rotX: degToRad(270),
			rotZ: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -4,
			y: 1.3,
			z: 5,
			rotX: degToRad(270),
			rotZ: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -4,
			y: 1.3,
			z: -7,
			rotX: degToRad(270),
			rotZ: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -7,
			y: 1.3,
			z: -10,
			rotX: degToRad(270),
			rotZ: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -7,
			y: 1.3,
			z: -13,
			rotX: degToRad(270),
			rotZ: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -7,
			y: 1.3,
			z: -16,
			rotX: degToRad(270),
			rotZ: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -7,
			y: 1.3,
			z: -19,
			rotX: degToRad(270),
			rotZ: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -5,
			y: 1.3,
			z: -23,
			rotX: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -4,
			y: 4.3,
			z: -1,
			rotX: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -4,
			y: 4.3,
			z: -4,
			rotX: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -4,
			y: 4.3,
			z: 2,
			rotX: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -4,
			y: 4.3,
			z: 5,
			rotX: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -4,
			y: 4.3,
			z: -7,
			rotX: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -7,
			y: 4.3,
			z: -10,
			rotX: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -7,
			y: 4.3,
			z: -13,
			rotX: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -7,
			y: 4.3,
			z: -16,
			rotX: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -7,
			y: 4.3,
			z: -19,
			rotX: degToRad(270)
		},
		{
			scale: [0.15, 0.15, 0.15],
			x: -5,
			y: 4.3,
			z: -23,
			rotX: degToRad(270)
		}
	];
	app.pos.house2 = [
		{
			scale: [0.06, 0.06, 0.06],
			x: 6.5,
			y: 0,
			z: -1,
			rotY: degToRad(270)
		},
		{
			scale: [0.06, 0.06, 0.06],
			x: 5.5,
			y: 0,
			z: -24,
			rotY: degToRad(270)
		}
	];
	app.pos.bank = [
		{
			scale: [0.006, 0.006, 0.006],
			x: 6,
			y: 0,
			z: -10,
			rotY: degToRad(225)
		}
	];


/*********************/
/* UNTOUCHABLA STUFF */
/*********************/
var gameRunningInterval = null;
// Global variable definitionvar canvas;
var canvas;
var gl;
var shaderProgram;

// Buffers
var worldVertexPositionBuffer = null;
var worldVertexTextureCoordBuffer = null;

var cubeVertexPositionBuffer = null;
var cubeVertexTextureCoordBuffer = null;
var cubeVertexIndexBuffer = null;

// Model-view and projection matrix and model-view matrix stack
var mvMatrixStack = [];
var mvMatrix = mat4.create();
var pMatrix = mat4.create();

// Variables for storing textures
var textures = {}; // <- za dostop do tekstur, npr textures[wallTexture] ali pa textures.wallTexture
var wallTexture = "wallTexture"; // inicializiras tako da zaradi dostopa do njih^^
var cubeTexture = "cubeTexture";
var laraTexture = "laraTexture";
var alfredTexture = "alfredTexture";
var anaTexture = "anaTexture";
var lichkingTexture = "lichkingTexture";
var zidTexture = "zidTexture";
var mekTexture = "mekTexture";
var trashTexture = "trashTexture";
var backgroundTexture = "backgroundTexture";
var klopTexture = "klopTexture";
//var lucTexture = "lucTexture";
var carTexture = "carTexture";
var houseTexture = "houseTexture";
var house2Texture = "house2Texture";
var bankTexture = "bankTexture";



// Variable that stores  loading state of textures.
var texturesLoaded = 0;
var texturesN = 0;

// Keyboard handling helper variable for reading the status of keys
var currentlyPressedKeys = {};

// Variables for storing current position and speed
var xPosition = 0;
var yPosition = 0;
var zPosition = 0;
var speedX = 0;
var speedZ = 0;

// Used to make us "jog" up and down as we move forward.
var joggingAngle = 0;

// Helper variable for animation
var lastTime = 0;

//
// Matrix utility functions
//
// mvPush   ... push current matrix on matrix stack
// mvPop    ... pop top matrix from stack
// degToRad ... convert degrees to radians
//
function mvPushMatrix() {
	var copy = mat4.create();
	mat4.set(mvMatrix, copy);
	mvMatrixStack.push(copy);
}

function mvPopMatrix() {
	if (mvMatrixStack.length == 0) {
		throw "Invalid popMatrix!";
	}
	mvMatrix = mvMatrixStack.pop();
}

function degToRad(degrees) {
	return degrees * Math.PI / 180;
}

//
// initGL
//
// Initialize WebGL, returning the GL context or null if
// WebGL isn't available or could not be initialized.
//
function initGL(canvas) {
	var gl = null;
	try {
		// Try to grab the standard context. If it fails, fallback to experimental.
		gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
		gl.viewportWidth = canvas.width;
		gl.viewportHeight = canvas.height;
	} catch(e) {}

	// If we don't have a GL context, give up now
	if (!gl) {
		alert("Unable to initialize WebGL. Your browser may not support it.");
	}
	return gl;
}

//
// getShader
//
// Loads a shader program by scouring the current document,
// looking for a script with the specified ID.
//
function getShader(gl, id) {
	var shaderScript = document.getElementById(id);

	// Didn't find an element with the specified ID; abort.
	if (!shaderScript) {
		return null;
	}

	// Walk through the source element's children, building the
	// shader source string.
	var shaderSource = "";
	var currentChild = shaderScript.firstChild;
	while (currentChild) {
		if (currentChild.nodeType == 3) {
			shaderSource += currentChild.textContent;
		}
		currentChild = currentChild.nextSibling;
	}

	// Now figure out what type of shader script we have,
	// based on its MIME type.
	var shader;
	if (shaderScript.type == "x-shader/x-fragment") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else if (shaderScript.type == "x-shader/x-vertex") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else {
		return null;  // Unknown shader type
	}

	// Send the source to the shader object
	gl.shaderSource(shader, shaderSource);

	// Compile the shader program
	gl.compileShader(shader);

	// See if it compiled successfully
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert(gl.getShaderInfoLog(shader));
		return null;
	}

	return shader;
}

//
// initShaders
//
// Initialize the shaders, so WebGL knows how to light our scene.
//
function initShaders() {
	var fragmentShader = getShader(gl, "shader-fs");
	var vertexShader = getShader(gl, "shader-vs");

	// Create the shader program
	shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);

	// If creating the shader program failed, alert
	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		alert("Unable to initialize the shader program.");
	}

	// start using shading program for rendering
	gl.useProgram(shaderProgram);

	// store location of aVertexPosition variable defined in shader
	shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");

	// turn on vertex position attribute at specified position
	gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

	// store location of aVertexNormal variable defined in shader
	shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");

	// store location of aTextureCoord variable defined in shader
	gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);

	// store location of uPMatrix variable defined in shader - projection matrix 
	shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
	// store location of uMVMatrix variable defined in shader - model-view matrix 
	shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
	// store location of uSampler variable defined in shader
	shaderProgram.samplerUniform = gl.getUniformLocation(shaderProgram, "uSampler");

	// store location of uHasTexture variable defined in shader
	shaderProgram.hasTexture = gl.getUniformLocation(shaderProgram, "uHasTexture");
	// store location of uColor variable defined in shader
	shaderProgram.modelColor = gl.getUniformLocation(shaderProgram, "uColor");
}

//
// setMatrixUniforms
//
// Set the uniforms in shaders.
//
function setMatrixUniforms() {
	gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
	gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

//
// initTextures
//
// Initialize the textures we'll be using, then initiate a load of
// the texture images. The handleTextureLoaded() callback will finish
// the job; it gets called each time a texture finishes loading.
//
function initTextures() {

	// Ostale teksture potrebne za igro
	initTexture(wallTexture, "./assets/room_floor.jpg", "LINEAR");

	initTexture(cubeTexture, "./assets/patrik_skin.jpg", "MIPMAP");

	// Lich King
	initTexture(laraTexture, "./assets/frame006066770.png", "MIPMAP");
	// lara
	initTexture(lichkingTexture, "./assets/ARTHASLICHKING_01.png", "MIPMAP");
	initTexture(alfredTexture, "./assets/alfred_dif.png", "MIPMAP");
	initTexture(anaTexture, "./assets/Ana_dif.png", "MIPMAP");
	initTexture(zidTexture, "./assets/CinderblockSloppy0005_1_S.jpg", "MIPMAP");
	initTexture(mekTexture, "./assets/difpart1.jpg", "LINEAR");
	initTexture(trashTexture, "./assets/metal.jpg", "LINEAR");
	initTexture(backgroundTexture, "./assets/backgroundImage.jpg", "LINEAR");
	initTexture(klopTexture, "./assets/walnut.jpg");
	//initTexture(lucTexture, "./assets/wall.png");
	initTexture(carTexture, "./assets/carbodyD.png");
	initTexture(houseTexture, "./assets/kuca.jpg");
	initTexture(house2Texture, "./assets/Build11.jpg");
	initTexture(bankTexture, "./assets/domik.jpg");
	
}

function initTexture(textureName, src, type) {// type: "LINEAR" ali "MIPMAP"!!
	texturesN += 1;
	textures[textureName] = gl.createTexture();
	textures[textureName].image = new Image();
	textures[textureName].image.onload = function() {
		handleTextureLoaded(textures[textureName], type);
	};  // async loading
	textures[textureName].image.src = src;
}

function handleTextureLoaded(texture, type) {
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

	// Third texture usus Linear interpolation approximation with nearest Mipmap selection
	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	if (type === "LINEAR") {
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	} else if (type === "MIPMAP") {
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.MIPMAP);
	}	
	gl.generateMipmap(gl.TEXTURE_2D);

	gl.bindTexture(gl.TEXTURE_2D, null);

	// when texture loading is finished we can draw scene.
	texturesLoaded++;
}

//
// handleLoadedWorld
//
// Initialisation of world 
//
function handleLoadedWorld(data) {
	var lines = data.split("\n");
	var vertexCount = 0;
	var vertexPositions = [];
	var vertexTextureCoords = [];
	for (var i in lines) {
		var vals = lines[i].replace(/^\s+/, "").split(/\s+/);
		if (vals.length == 5 && vals[0] != "//") {
			// It is a line describing a vertex; get X, Y and Z first
			vertexPositions.push(parseFloat(vals[0]));
			vertexPositions.push(parseFloat(vals[1]));
			vertexPositions.push(parseFloat(vals[2]));

			// And then the texture coords
			vertexTextureCoords.push(parseFloat(vals[3]));
			vertexTextureCoords.push(parseFloat(vals[4]));

			vertexCount += 1;
		}
	}

	worldVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexPositions), gl.STATIC_DRAW);
	worldVertexPositionBuffer.itemSize = 3;
	worldVertexPositionBuffer.numItems = vertexCount;

	worldVertexTextureCoordBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexTextureCoordBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexTextureCoords), gl.STATIC_DRAW);
	worldVertexTextureCoordBuffer.itemSize = 2;
	worldVertexTextureCoordBuffer.numItems = vertexCount;
}

//  ------- draw object -------
function drawObjects(model, color, texture, posMatrix) {

	if(posMatrix !== undefined)
	{//	Nadaljuj samo, če so dejansko dodani modeli tudi v igro z ustreznimi pozicijami
	//	definiranimi v "app.pos" spremenljivki

		gl.useProgram(shaderProgram);

		gl.bindBuffer(gl.ARRAY_BUFFER, model.vertexBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, model.vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);

		gl.bindBuffer(gl.ARRAY_BUFFER, model.textureBuffer);
		gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, model.textureBuffer.itemSize, gl.FLOAT, false, 0, 0);

		//gl.uniform4fv(shaderProgram.modelColor, color);
		if(texture !== null) {
			gl.activeTexture(gl.TEXTURE0);
			gl.bindTexture(gl.TEXTURE_2D, texture);
			gl.uniform1i(shaderProgram.samplerUniform, 0);
			gl.uniform1i(shaderProgram.hasTexture, true);
		}
		else {
			gl.uniform1i(shaderProgram.hasTexture, false);
			gl.uniform4fv( shaderProgram.modelColor, color );
		}
		posMatrix.forEach(function(pos){
			mvPushMatrix();

			if(pos.scale !== undefined){
				mat4.scale(mvMatrix, pos.scale);
			}
			if(pos.x === undefined){
				pos.x = 0;
			}
			if(pos.y === undefined){
				pos.y = 0;
			}
			if(pos.z === undefined){
				pos.z = 0;
			}
			mat4.translate(mvMatrix, [pos.x / pos.scale[0], pos.y / pos.scale[1], pos.z / pos.scale[2]]);

			if(pos.rotX !== undefined){
				mat4.rotateX(mvMatrix, pos.rotX);
			}
			if(pos.rotY !== undefined){
				mat4.rotateY(mvMatrix, pos.rotY);
			}
			if(pos.rotZ !== undefined){
				mat4.rotateZ(mvMatrix, pos.rotZ);
			}

			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, model.indexBuffer);
			setMatrixUniforms();
			gl.drawElements(gl.TRIANGLES, model.indexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

			mvPopMatrix();
		});
	}
}

//
// loadWorld
//
// Loading world 
//
function loadWorld() {
	var request = new XMLHttpRequest();
	request.open("GET", "./assets/world.txt");
	request.onreadystatechange = function () {
		if (request.readyState == 4) {
			handleLoadedWorld(request.responseText);
		}
	}
	request.send();
}

//
// drawScene
//
// Draw the scene.
//
function drawScene() {
	
	// set the rendering environment to full canvas size
	gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
	// Clear the canvas before we start drawing on it.
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// If buffers are empty we stop loading the application.
	if (worldVertexTextureCoordBuffer == null || worldVertexPositionBuffer == null) {
		return;
	}
  
	// Establish the perspective with which we want to view the
	// scene. Our field of view is 45 degrees, with a width/height
	// ratio of 640:480, and we only want to see objects between 0.1 units
	// and 100 units away from the camera.
	mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);

	// Set the drawing position to the "identity" point, which is
	// the center of the scene.
	mat4.identity(mvMatrix);
	
	// Move the camera view backwards
	mat4.translate(mvMatrix, [0, -cameraUp, -cameraBack]);



	/***** DRAW THE PLAYER ****/
	drawObjects(app.meshes.actor, [0.0, 1.0, 0.0, 1.0], textures.cubeTexture, app.pos.player);
	
	// Move the rest of the scene relative to the player!
	mat4.translate(mvMatrix, [xPosition, yPosition, zPosition]);
	
	// ------- Izris ostalih udeležencev v sceni -------------
	// Predzadnji param. zgornje funkcije je textura. Če je null, potem uposteva barvo
	drawObjects(app.meshes.lara, [0.0, 1.0, 0.0, 1.0], textures[laraTexture], app.pos.lara);
	drawObjects(app.meshes.lichking, [0.0, 1.0, 0.0, 1.0], textures[lichkingTexture], app.pos.lichking);
	drawObjects(app.meshes.alfred, [0.0, 1.0, 0.0, 1.0], textures[alfredTexture], app.pos.alfred);
	drawObjects(app.meshes.ana, [0.0, 1.0, 0.0, 1.0], textures[anaTexture], app.pos.ana);
	drawObjects(app.meshes.zid, [0.0, 1.0, 0.0, 1.0], textures[zidTexture], app.pos.zid);
	drawObjects(app.meshes.mek, [0.0, 1.0, 0.0, 1.0], textures[mekTexture], app.pos.mek);
	drawObjects(app.meshes.trash, [0.0, 1.0, 0.0, 1.0], textures[trashTexture], app.pos.trash);
	drawObjects(app.meshes.klop, [0.0, 1.0, 0.0, 1.0], textures[klopTexture], app.pos.klop);
	//drawObjects(app.meshes.luc, [0.0, 1.0, 0.0, 1.0], textures[lucTexture], app.pos.luc);
	drawObjects(app.meshes.car, [0.0, 1.0, 0.0, 1.0], textures[carTexture], app.pos.car);
	drawObjects(app.meshes.house, [0.0, 1.0, 0.0, 1.0], textures[houseTexture], app.pos.house);
	drawObjects(app.meshes.house2, [0.0, 1.0, 0.0, 1.0], textures[house2Texture], app.pos.house2);
	drawObjects(app.meshes.bank, [0.0, 1.0, 0.0, 1.0], textures[bankTexture], app.pos.bank);
	
	// -----------------------------------------------------
	
	/* IZRIS SVETA */
	
	// Activate textures
	mvPushMatrix();
	gl.uniform1i(shaderProgram.hasTexture, true);
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, textures.wallTexture);
	gl.uniform1i(shaderProgram.samplerUniform, 0);

	// Set the texture coordinates attribute for the vertices.
	gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexTextureCoordBuffer);
	gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, worldVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

	// Draw the world by binding the array buffer to the world's vertices
	// array, setting attributes, and pushing it to GL.
	gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexPositionBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, worldVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

	// Draw the world.
	mat4.translate(mvMatrix, [0, 0, -7]);
	setMatrixUniforms();
	gl.drawArrays(gl.TRIANGLES, 0, worldVertexPositionBuffer.numItems);
	mvPopMatrix();
}

//
// animate
//
// Called every time before redrawing the screen.
//
function animate() {
	var timeNow = new Date().getTime();
	if (lastTime != 0) {
		var elapsed = timeNow - lastTime;
		
		/*
		if(elapsed > (2000/fps)){
			elapsed = 0;
		}
		*/
		
		moveObjects(elapsed);			// Premika premikajoče se objekte, ki niso Player
		
		randomActerMovement(elapsed);	// Izračuna vrednosti za "pijano" premikanje Player-ja
		applyActerMovement(elapsed);	// Dejansko premakne Player-ja (in kamero)
		
		// Biu je * 0.3
		joggingAngle += elapsed * 0.3; // 0.6 "fiddle factor" - makes it feel more realistic :-)
		yPosition = -Math.sin(degToRad(joggingAngle)) / 40; // Biu je 50
	}
	lastTime = timeNow;
}
//
// moveObjects
//
// Called every frame to move predefined objects for a specified vector
//
function moveObjects(elapsed){
	// Premakni Alfreda
	moveObject(
		// Izračuna premike elementov
		app.pos.car[0],
		movementSpeedPesecX*9 * elapsed,
		movementSpeedPesecZ*9 * elapsed
	);
}
//
// moveObject
//
// Funkcija, ki dejansko premakne objekt, in si hkrati zapomne njegovo staro lokacijo,
// da si lahko z njo pomagamo pri Collision Detection za premik Player-ja
//
function moveObject(whome, howX, howZ){
	
	// Backup prejšnje pozicije - pomoč pri preusmeritvi Player-ja
	whome.oldZ = whome.z;
	whome.oldX = whome.x;
	
	// Premakni Player-ja
	whome.z += howZ;
	whome.x += howX;
}

//
// randomActerMovement
//
// Kliče se vsakič znotraj funkcije animate, ki se kliče vsakič pred ponovnim izrisom
// Funkcija poskrbi za to, da ima akter nenadne premike v random smereh
//
var randTimely = 0, weight = 1;
var acterRandVecs = [];

var vecsTransitionTimeDefault = 500; // Transitional time in miliseconds between changing vectors
var vecsTransitionTime = vecsTransitionTimeDefault;
var vecsElapsed = 0;
function randomActerMovement(elapsed) {
	
	if(randTimely <= 0)
	{// Calculates the new random movement
		
		randTimely = 500 + Math.floor(Math.random() * 1400);
		vecsTransitionTimeDefault = randTimely;
		
		var randAngle = degToRad(Math.ceil(Math.random() * 360));	// Izračuna kot, maksimalen 360°
																	// In ga pretvori v Radiane
		// Izračunaj moč impulznega premika na intervalu [0,1]
		var randImpulse = 0.5 + Math.random() / 2;
		
		var vec = {};
		
		if (Math.random() >= 0.5) // Določi, kdaj naj akterja random premika
		{ // popravis na ~0.6 pred oddajo (da ga samo učasih zanaša)
			vec.x = Math.cos(randAngle) * randImpulse * maxMovementSpeedX;
			vec.z = Math.sin(randAngle) * randImpulse * maxMovementSpeedZ;
		} else{
			vec.x = 0;
			vec.z = 0;
		}
		
		acterRandVecs.push(vec);
	}
	
	randTimely -= elapsed;
}
function applyActerMovement(elapsed){
	// Applies the random movements onto the actor
	
	if(acterRandVecs.length > 0){
		if(vecsTransitionTime >= 0 && acterRandVecs.length > 1)
		{// Imam več kot 1 vektor gibanja, zato izvedi gladkejše spreminjanje med njima
			
			// Izračunaj utež med prvima dvema vektorjema gibanja, ostale zanemari
			var x2 = (vecsTransitionTime/500) * (vecsTransitionTime/500);
			weight = Math.sqrt( 1 - x2 );
			
			// Upoštevaj prva dva vektorja gibanja na hitrost in smer Player-ja
			speedX += acterRandVecs[0].x * weight;
			speedX += acterRandVecs[1].x * (1 - weight);
			speedZ += acterRandVecs[0].z * weight;
			speedZ += acterRandVecs[1].z * (1 - weight);
			
		} else if(acterRandVecs.length > 1)
		{// Imam več kot 1 vektor gibanja, ampak gladkejše spreminjanje med njima sem že opravil
			// Ponastavi čas za dolžino naslednjega gladkega spreminjanja vektorjev gibanja
			vecsTransitionTime = vecsTransitionTimeDefault;
			
			// Odstrani vse odvečne vektorje gibanja in ohrani samo zadnjega
			while(acterRandVecs.length > 1)
				acterRandVecs.shift();
			
			// Upoštevaj vektorj gibanja na hitrost in smer Player-ja
			speedX += acterRandVecs[0].x;
			speedZ += acterRandVecs[0].z;
		} else {
			// Imam samo en vektor gibanja, upoštevaj ga na hitrost in smer Player-ja
			speedX += acterRandVecs[0].x;
			speedZ += acterRandVecs[0].z;
		}
	}
	// Znižaj čas, v katerem se bo še izvajalo gladko spreminjanje vektorja gibanja
	vecsTransitionTime -= elapsed;
	
	// Omejitev maksimalne hitrosti premikanja v eno smer
	// Normalen človek ne more leteti, četudi je pijan
	if(speedX > maxMovementSpeedX)
		speedX = maxMovementSpeedX;
	if(speedX < -maxMovementSpeedX)
		speedX = -maxMovementSpeedX;
	
	if(speedZ > maxMovementSpeedZ)
		speedZ = maxMovementSpeedZ;
	if(speedZ < -maxMovementSpeedZ)
		speedZ = -maxMovementSpeedZ;
	
	// Preveri, če se je Player zabil v kakeršenkoli drug objekt/osebo
	checkCollisions(elapsed);
}
function checkCollisions(elapsed)
{// Izračuna trenuten položaj Player-ja, upošteva če se na neko mesto sploh lahko premakne
	
	// Izračunaj želeno spremembo položaja Player-ja
	var deltaX = speedX * elapsed;
	var deltaZ = speedZ * elapsed;
	
	// Izračunaj želen dejanski položaj
	var pX = xPosition + deltaX;
	var pZ = zPosition + deltaZ;
	
	// Negiraj vrednosti, ker jih primerjaš s položajem kamere
	pX = -pX;
	pZ = -pZ - cameraBack; // Upoštevaj tudi odmik kamere od Player-ja
	
	for(var index in app.pos)
	{// Preglej za vse pozicijske objekte v app.pos
		
		if(app.pos.hasOwnProperty(index))
		{// Preverjanje, če ta index res obstaja v app.pos (da slučajno ne pride do problemov)
			
			if(index !== "player")
			{// Če je ta pos objekt, ki hrani samo scale o Player-ju, ignoriraj!
				
				app.pos[index].forEach(function(obj)
				{// Obravnavaj vse pojavitve tega objekta v sceni
					
					if(obj.limitX !== undefined && obj.limitZ !== undefined)
					{
						// Izračunaj središče omejitvenega prostora, če je nastavljen offset
						var tmpZ = obj.z;
						if(obj.offsetZ !== undefined){
							tmpZ += obj.offsetZ;
						}
						var tmpX = obj.x;
						if(obj.offsetX !== undefined){
							tmpX += obj.offsetX;
						}

						// Izračunaj omejitvene kote omejitvenega prostora premikanja Player-ja
						// Torej prostor, v katerem se zgodi Collision
						var minZ = tmpZ - obj.limitZ;
						var maxZ = tmpZ + obj.limitZ;
						var minX = tmpX - obj.limitX;
						var maxX = tmpX + obj.limitX;
						
						// Preveri, če je želena pozicija znotraj zgornjega prostora
						if(
							minZ <= pZ && pZ <= maxZ &&
							minX <= pX && pX <= maxX
						){
							if (index === "lichking") {
								log("Cigarette obtained. You win!");
								winGame();
							} else {
								log("Collided! -"+index);
							}

							// Izračunaj pozicije Player-ja, v kolikor se je zgodil Collision
							var tmpPz = pZ + deltaZ;
							var tmpPx = pX + deltaX;


							if(obj.oldX !== undefined){

								if(
									minZ < pZ && pZ < maxZ &&
									minX < tmpPx && tmpPx < maxX
								){// Ali se je zaletel v prednjo/zadnjo stranico omejitvenega prostora?

									if(obj.oldZ !== undefined){
										var tZ = obj.oldZ;
										if(obj.offsetZ !== undefined){
											tZ += obj.offsetZ;
										}
										deltaZ = tZ - tmpZ - 0.00001;
									} else{
										deltaZ = 0;
									}

								}

								// Ker smo (mogoče) spreminjali deltaZ, ponovi izračun
								var tmpPz = pZ - deltaZ;

								if(
									minZ < tmpPz && tmpPz < maxZ &&
									minX < pX && pX < maxX
								){// Ali se je zaletel v eno izmed stranskih stranic omejitvenega prostora?

									if(obj.oldX !== undefined){
										var tX = obj.oldX;
										if(obj.offsetX !== undefined){
											tX += obj.offsetX;
										}
										deltaX = tX - tmpX;
									} else{
										deltaX = 0;
									}

								}
							} else
							{// Uporabi staro Collision Detection, če je predmet stacionaren!
								if(
									minZ < pZ && pZ < maxZ &&
									minX < tmpPx && tmpPx < maxX
								){// Ali se je zaletel v prednjo/zadnjo stranico omejitvenega prostora?

									deltaZ = 0;

								}
								if(
									minZ < tmpPz && tmpPz < maxZ &&
									minX < pX && pX < maxX
								){// Ali se je zaletel v eno izmed stranskih stranic omejitvenega prostora?

									deltaX = 0;

								}
							}
						}
					}
				});
			}
		}
	}
	
	// Preveri, če je znotraj imejene površine gibanja
	if (pX > limitMovementX)
		deltaX = 0;
	if (pX < -limitMovementX)
		deltaX = 0;
	if (pZ > limitMovementBack)
		deltaZ = 0;
	if (pZ < limitMovementForward)
		deltaZ = 0;
	// Nastavi izračunano in preverjeno novo pozicijo Player-ja
	xPosition = xPosition + deltaX;
	zPosition = zPosition + deltaZ;
}

//
// Keyboard handling helper functions
//
// handleKeyDown    ... called on keyDown event
// handleKeyUp      ... called on keyUp event
//
function handleKeyDown(event) {
	// storing the pressed state for individual key
	currentlyPressedKeys[event.keyCode] = true;
}

function handleKeyUp(event) {
	// reseting the pressed state for individual key
	currentlyPressedKeys[event.keyCode] = false;
}

//
// handleKeys
//
// Called every time before redrawing the screen for keyboard
// input handling. Function continuisly updates helper variables.
//
function handleKeys() {
	
	if(currentlyPressedKeys[27]){
		pauseGame();
	}
	
	if (currentlyPressedKeys[37] || currentlyPressedKeys[65]) {
		// Left cursor key or A
		speedX =  maxMovementSpeedX;
	} else if (currentlyPressedKeys[39] || currentlyPressedKeys[68]) {
		// Right cursor key or D
		speedX = -maxMovementSpeedX;
	} else {
		speedX = 0;
	}

	if (currentlyPressedKeys[38] || currentlyPressedKeys[87]) {
		// Up cursor key or W
		speedZ =  maxMovementSpeedZ;
	} else if (currentlyPressedKeys[40] || currentlyPressedKeys[83]) {
		// Down cursor key
		speedZ = -maxMovementSpeedZ;
	} else {
		speedZ = 0;
	}
}

//
// start
//
// Called when the canvas is created to get the ball rolling.
// Figuratively, that is. There's nothing moving in this demo.
//
function start(meshes) {
	canvas = document.getElementById("glcanvas");

	gl = initGL(canvas);      // Initialize the GL context

	// Only continue if WebGL is available and working
	if (gl) {
		
		// Begin setting up the rest of the canvas
		gl.clearColor(0.0, 0.0, 0.0, 0.0);                      // Set clear color to black, fully opaque
		gl.clearDepth(1.0);                                     // Clear everything
		gl.enable(gl.DEPTH_TEST);                               // Enable depth testing
		gl.depthFunc(gl.LEQUAL);                                // Near things obscure far things

		// Initialize the shaders; this is where all the lighting for the
		// vertices and so forth is established.
		initShaders();

		// Next, load and set up the textures we'll be using.
		initTextures();

		// ------ inicializacija bufferjeu za asset actor ---------
		app.meshes = meshes;
		OBJ.initMeshBuffers(gl, app.meshes.actor);

		OBJ.initMeshBuffers(gl, app.meshes.lara);
		OBJ.initMeshBuffers(gl, app.meshes.lichking);
		OBJ.initMeshBuffers(gl, app.meshes.alfred);
		OBJ.initMeshBuffers(gl, app.meshes.ana);
		OBJ.initMeshBuffers(gl, app.meshes.zid);
		OBJ.initMeshBuffers(gl, app.meshes.mek);
		OBJ.initMeshBuffers(gl, app.meshes.trash);
		OBJ.initMeshBuffers(gl, app.meshes.klop);
		//OBJ.initMeshBuffers(gl, app.meshes.luc);
		OBJ.initMeshBuffers(gl, app.meshes.car);
		OBJ.initMeshBuffers(gl, app.meshes.house);
		OBJ.initMeshBuffers(gl, app.meshes.house2);
		OBJ.initMeshBuffers(gl, app.meshes.bank);

		// Initialise world objects
		loadWorld();

		// Bind keyboard handling functions to document handlers
		document.onkeydown = handleKeyDown;
		document.onkeyup = handleKeyUp;
	}
}
// Look up text canvas
var textCanvas = document.getElementById("text");
// make a 2D context for it
var ctx2d = textCanvas.getContext("2d");

function openMenu(){
	log("Displaying main menu");
	
	// Clear the 2D canvas
	ctx2d.clearRect(0, 0, ctx2d.canvas.width, ctx2d.canvas.height);

	// Change FONT
	ctx2d.font = 'italic 40pt Calibri';
	ctx2d.fillStyle = 'white';
	
	ctx2d.strokeStyle = 'black';
	ctx2d.lineWidth = 2;
	
	// Fill up text
	ctx2d.fillText("Loading...", 50, 500);
	ctx2d.strokeText("Loading...", 50, 500);
	
	var textureLoaded = false;
	
	setTimeout(function(){
		gameRunningInterval = setInterval(function(){
			
			if(texturesLoaded == texturesN){
				
				if(!textureLoaded){
					log("Teksture naložene!");
					textureLoaded = true;
				}
				
				// Clear the 2D canvas
				ctx2d.clearRect(0, 0, ctx2d.canvas.width, ctx2d.canvas.height);
				
				ctx2d.fillText("Press ENTER to start the game...", 50, 500);
				ctx2d.strokeText("Press ENTER to start the game...", 50, 500);
				
				if(currentlyPressedKeys[13]){
					ctx2d.clearRect(0, 0, ctx2d.canvas.width, ctx2d.canvas.height);
					
					clearInterval(gameRunningInterval);
					gameRunningInterval = null;
					
					runGame();
				}
			} else{
				log("Teksture se še nalagajo... (" + texturesLoaded + "/" + texturesN + ")");
			}
		}, 50);
	}, 100);
	
}
function pauseGame(){
	clearInterval(gameRunningInterval);
	gameRunningInterval = null;
	
	ctx2d.fillStyle = "white";
	ctx2d.font = 'italic 50pt Calibri';
	
	
	ctx2d.strokeStyle = 'black';
	ctx2d.lineWidth = 1;
	
	ctx2d.fillText("Game paused: Press ENTER to continue...", 90, 300);
	ctx2d.strokeText("Game paused: Press ENTER to continue...", 90, 300);
	
	gameRunningInterval = setInterval(function(){
		if(currentlyPressedKeys[13]){
			ctx2d.clearRect(0, 0, ctx2d.canvas.width, ctx2d.canvas.height);
			
			clearInterval(gameRunningInterval);
			gameRunningInterval = null;
			
			runGame();
			
			lastTime = 0;
		}
	}, 50);
}
function winGame(){
	
	clearInterval(gameRunningInterval);
	gameRunningInterval = null;
	
	ctx2d.fillStyle = "green";
	// Fill up text
	
	ctx2d.font = 'bold 60pt Calibri';
	
	ctx2d.strokeStyle = 'black';
	ctx2d.lineWidth = 1;
	
	ctx2d.fillText("You've found a cigarette! You WIN!", 50, 350);
	ctx2d.strokeText("You've found a cigarette! You WIN!", 50, 350);
}
function runGame(){
	log("Game starting up");
	if(texturesLoaded == texturesN && gameRunningInterval === null){
		
		gameRunningInterval = setInterval(function() {
			
			handleKeys();	
			requestAnimationFrame(animate);
			drawScene();
			
		}, Math.floor(1000 / fps));
	}
}

// --------- za loadanje assetov
function loadMeshes() {
	
	if(gameRunningInterval === null)
	{
		// Display main menu
		openMenu();
	}
	
	OBJ.downloadMeshes({
		'actor': './assets/Patrick.obj', // located in the models folder on the server
		'lara': './assets/frame00606677.obj',
		'lichking': './assets/Lich_King.obj',
		'alfred': './assets/Alfred1.obj',
		'ana': './assets/Ana1.obj',
		'zid': './assets/Wall.obj',
		'mek': './assets/part1model.obj',
		'trash': './assets/Trash Bin.obj',
		'klop': './assets/park_bench1.obj',
		'luc': './assets/Street Light.obj',
		'car': './assets/Small car.obj',
		'house': './assets/small_house.obj',
		'house2': './assets/Build1.obj',
		'bank': './assets/bank.obj',
	}, start);
}
function log(content){
	var logger = document.getElementById("logger");
	logger.innerHTML = content + "<br>" + logger.innerHTML;
	//logger.innerHTML = content;
}
