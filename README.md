# Midnight Stroll #
## Uvod ##
Projektna naloga študentov Armin Makovec, Demian Bucik in Matjaž Pogorelec pri predmetu **Računalniška Grafika in Tehnologija Iger**.
Izdelana na **Fakulteti za Računalništvo in Informatika**, *Univerza v Ljubljani*.

## Opis ##
Po končanem žuru se vračaš domov. Ker si se prenapil pred spancem potrebuješ cigareto.
Najdi *Undead Lich King*, da od njega prejmeš cigareto in lahko v miru zaspiš (končaš igro) v čim krajšem času.

## Kontrole ###
* ESC - Pavza igre, vračanje v glavni meni (iz pavze)
* W - pomikanje naprej
* S - pomikanje nazaj
* A - pomikanje v levo stran
* D - pomikanje v desno stran
* Enter - Začetek igre, Nadaljevanje po pavzi, ponovni zagon po koncu igre.

## Uporabljane tehnologije ##
* HTML5 - Uporabljan Canvas za pripravo menijev in ozadja
* WebGL - Nalaganje in izrisovanje 3D modelov
* Autodesk Maya - Poprava modelov 3D (modeli so iz spleta, samo malenkosti popravljene, da se pravilno nalagajo v igri)

## Zadolžitve ##
* Priprava modelov in sveta: Matjaž Pogorelec
* Priprava logike igre: Armin Makovec in Demian Bucik
* Priprava logike gibanja: Armin Makovec
* Priprava menijev: Armin Makovec
* Končni popravki: Demian Bucik in Armin Makovec.

## Dependencies ##
* GL Matrix - 2.4.0
* gL Matrix - 0.9.5
* WebGL Obj Loader
* WebGL Utils